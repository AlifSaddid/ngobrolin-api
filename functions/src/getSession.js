const faunadb = require('faunadb');
const q = require('faunadb').query;

const client = new faunadb.Client(
    { 
        secret: process.env.FAUNA_SECRET,
        domain: "db.us.fauna.com" 
    }
)
const OpenTok = require("opentok");
const OT = new OpenTok(process.env.VONAGE_KEY, process.env.VONAGE_SECRET);

const headers = {
    'access-control-allow-origin': '*',
    'access-control-allow-headers': '*'
}

exports.handler = async (event, context) => {
    try {
        if (event.httpMethod == 'OPTIONS') {
            return {
                headers: {
                    ...headers,
                    'Allow': 'POST'
                },
                statusCode: 204
            }
        }

        const { name } = JSON.parse(event.body);
        
        
        const document = await client.query(
            q.Get(
                q.Match(
                    q.Index('sessions_by_name'), name
                )
            )
        );

        const token = OT.generateToken(document.data.id, {
            role: 'publisher',
            data: `roomname=${document.data.name}`
        })

        return {
            headers,
            statusCode: 200,
            body: JSON.stringify({
                token,
                sessionId: document.data.id,
                apiKey: process.env.VONAGE_KEY
            })
        };
    } catch(e) {
        console.error('Error', e)
        return { 
            headers, 
            statusCode: 500, 
            body: 'Error: ' + e 
        }
    }
}