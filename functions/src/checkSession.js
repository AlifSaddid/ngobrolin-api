const faunadb = require('faunadb');
const q = require('faunadb').query;

const client = new faunadb.Client(
    { 
        secret: process.env.FAUNA_SECRET,
        domain: "db.us.fauna.com" 
    }
)
const OpenTok = require("opentok");
const OT = new OpenTok(process.env.VONAGE_KEY, process.env.VONAGE_SECRET);

const headers = {
    'access-control-allow-origin': '*',
    'access-control-allow-headers': '*'
}

exports.handler = async (event, context) => {
    try {
        if (event.httpMethod == 'OPTIONS') {
            return {
                headers: {
                    ...headers,
                    'Allow': 'POST'
                },
                statusCode: 204
            }
        }

        const { name } = JSON.parse(event.body);
        
        const doesSessionExists = await client.query(
            q.Exists(
                q.Match(
                    q.Index('sessions_by_name'), name
                )
            )
        );
        
        return {
            headers,
            statusCode: 200,
            body: JSON.stringify(doesSessionExists)
        }
    } catch(e) {
        console.error('Error', e)
        return { 
            headers, 
            statusCode: 500, 
            body: 'Error: ' + e 
        }
    }
}